% Inspired by:
%
%  https://support.astron.nl/confluence/display/~kruger/Confluence+to+latex+to+pdf
%  https://lsst-texmf.lsst.io
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{astron}[ASTRON document class]

% Handle various document types, adding appropriate warning messages.
% Document type is specified as a class option; if none is specified, we use "undefined".
\newif\if@hastype
\@hastypefalse
\newcommand{\docType}{Undefined}

\DeclareOption{note}{
  \newcommand{\docTypeDescription}{
    \fbox{%
      \begin{minipage}{\dimexpr\linewidth-2\fboxrule-2\fboxsep}
        \begin{center}
          \begin{small}
          This technical note expresses the opinions of the authors on the date of writing.\\
          It is not binding on the SDC Program, and may not accurately reflect future developments.
          \end{small}
        \end{center}
      \end{minipage}%
    }
  }

  \renewcommand{\docType}{Tech. Note}
  \@hastypetrue
}

\DeclareOption{proposed}{
  \newcommand{\docTypeDescription}{
    \fbox{%
      \begin{minipage}{\dimexpr\linewidth-2\fboxrule-2\fboxsep}
        \begin{center}
          \begin{small}
          This document proposes a change to the technical baseline of the SDC Program.\\
          It is not binding on the Program until approved by a change control process.
          \end{small}
        \end{center}
      \end{minipage}%
    }
  }

  \renewcommand{\docType}{Proposed}
  \@hastypetrue
}

\DeclareOption{baseline}{
  \newcommand{\docTypeDescription}{
    \fbox{%
      \begin{minipage}{\dimexpr\linewidth-2\fboxrule-2\fboxsep}
        \begin{center}
          \begin{small}
          This document is part of the technical baseline of the SDC Program.\\
          It is binding on the activities of the Program.\\
          It may only be modified through a formal change control process.
          \end{small}
        \end{center}
      \end{minipage}%
    }
  }

  \renewcommand{\docType}{Baseline}
  \@hastypetrue
}
\ProcessOptions\relax

\LoadClass[a4paper,12pt,twoside]{article}

\newlength{\topMargin}
\newlength{\bottomMargin}
\newlength{\sideMargin}
\setlength{\topMargin}{3.5cm}
\setlength{\bottomMargin}{3.5cm}
\setlength{\sideMargin}{2cm}

\RequirePackage[absolute]{textpos}
\RequirePackage{biblatex}
\RequirePackage{fancyhdr}
\RequirePackage{geometry}
\RequirePackage[pdfborder={0 0 0}]{hyperref}
\RequirePackage[nonumberlist,nogroupskip]{glossaries}
\RequirePackage{graphicx}
\RequirePackage{lastpage}
\RequirePackage{libertinus}
\RequirePackage{microtype}
\RequirePackage{parskip}
\RequirePackage{tabularx}
\RequirePackage{booktabs}
\RequirePackage[capitalize]{cleveref}

% Set up “clever” references.
%
% Use lower-case-p for pages references.
\crefname{page}{page}{pages}
% Abbreviations for appendix when not at the start of a sentence
\crefname{appendix}{Appx.}{Appxs.}
\Crefname{appendix}{Appendix}{Appendices}
% Use § for section references, except at the beginning of sentences.
\crefformat{section}{\S#2#1#3}
\crefrangeformat{section}{\S\S#3#1#4--#5#2#6}
\crefmultiformat{section}{\S\S#2#1#3}{ \&~#2#1#3}{, #2#1#3}{ \&~#2#1#3}
\Crefformat{section}{Section~#2#1#3}
\Crefrangeformat{section}{Sections~#3#1#4 to~#5#2#6}
\Crefmultiformat{section}{Sections~#2#1#3}{ and~#2#1#3}{, #2#1#3}{ and~#2#1#3}

% Set up glossaries
%
\input{astron-abbreviations}
\makeglossaries
% Don't expand these acronyms in the text.
\glsunset{API}
\glsunset{Apertif}
\glsunset{CPU}
\glsunset{FTE}
\glsunset{FTP}
\glsunset{GPU}
\glsunset{LOFAR}
\glsunset{NWO}
\glsunset{QA}
\glsunset{RDMA}
\glsunset{SSH}

% Link to document repositories for appropriate e-prints in the bibliography
\DeclareFieldFormat{eprint:sdc-handle}{%
  Doc handle\addcolon\space
    \href{https://git.astron.nl/swinbank/doc-#1}{#1}%
}

% Standard bibliography files included in this package
\addbibresource{astron.bib}
\addbibresource{general.bib}
\addbibresource{ivoa.bib}
\addbibresource{lofar.bib}
\addbibresource{sdc.bib}
\addbibresource{sdco.bib}

% No need for a fancy font for the URL
\urlstyle{same}

\geometry{tmargin=\topMargin,
          bmargin=\bottomMargin,
          lmargin=\sideMargin,
          rmargin=\sideMargin,
          headheight=52.05211pt}

\newcommand{\docTitle}{Set the document title with \texttt{\textbackslash{}setDocTitle}}
\newcommand{\setDocTitle}[1]{
   \renewcommand{\docTitle}{#1}
}

\newcommand{\docNumber}{Set the document number with \texttt{\textbackslash{}setDocNumber}}
\newcommand{\setDocNumber}[1]{
   \renewcommand{\docNumber}{#1}
}

\newcommand{\docRevision}{Set the document revision with \texttt{\textbackslash{}setDocRevision}}
\newcommand{\setDocRevision}[1]{
   \renewcommand{\docRevision}{#1}
}

\newcommand{\docDate}{Set the document date with \texttt{\textbackslash{}setDocDate}}
\newcommand{\setDocDate}[1]{
   \renewcommand{\docDate}{#1}
}

\newcommand{\docProgram}{Set the document program with \texttt{\textbackslash{}setDocProgram}}
\newcommand{\setDocProgram}[1]{
   \renewcommand{\docProgram}{#1}
}

%
% Document history
%
\providecommand{\addChangeRecord}[3]{ #1 & #2 & #3 \tabularnewline \hline}
\newcommand{\docChangeRecord}{
... & ... & ... \tabularnewline \hline
}
\newcommand{\setDocChangeRecord}[1]{\renewcommand{\docChangeRecord}
{
#1
}}

\newcommand{\changeRecord}{%
  \clearpage
  \section*{Document History}
  \begin{tabularx}{\textwidth}{|X|p{0.13\textwidth}|p{0.68\textwidth}|X|}
    \hline
    \textbf{Revision} & \textbf{Date} & \textbf{Description} \tabularnewline
    \hline\hline
    \docChangeRecord
  \end{tabularx}
  \clearpage
}

%
% Define authors, approvers, etc for the front page
%
\providecommand{\addPerson}[3]{ #1 & #2 & #3 \tabularnewline}

\newcommand{\docAuthors}{
... & ... & ... \tabularnewline
}
\newcommand{\setDocAuthors}[1]{\renewcommand{\docAuthors}
{
#1
}}

\newcommand{\docCheckers}{
... & ... & ... \tabularnewline
}
\newcommand{\setDocCheckers}[1]{\renewcommand{\docCheckers}
{
#1
}}

\newcommand{\docApprovers}{
... & ... & ... \tabularnewline
}
\newcommand{\setDocApprovers}[1]{\renewcommand{\docApprovers}
{
#1
}}

\newcommand{\docAuthorizers}{
... & ... & ... \tabularnewline
}
\newcommand{\setDocAuthorizers}[1]{\renewcommand{\docAuthorizers}
{
#1
}}

\newcommand{\authorTable}{%
  \begin{tabularx}{\textwidth}{|p{0.35\textwidth}|p{0.35\textwidth}|X|}
    \cline{2-3} \cline{3-3}
    \multicolumn{1}{c|}{} &
    \textbf{\footnotesize{}Organisatie / Organization} &
    \textbf{\footnotesize{}Datum / Date} \tabularnewline
    \hline
    \textbf{\footnotesize{}Auteur(s) / Author(s):\vspace{0.2cm}} & & \tabularnewline
    \docAuthors
    \hline
    \textbf{\footnotesize{}Controle / Checked:\vspace{0.2cm}} & & \tabularnewline
    \docCheckers
    \hline
    \textbf{\footnotesize{}Goedkeuring / Approval:\vspace{0.2cm}} & & \tabularnewline
    \docApprovers
    \hline
    \textbf{\footnotesize{}Autorisatie / Authorization:} & & \tabularnewline
    \docAuthorizers
    \textbf{\footnotesize{}Handtekening / Signature:\vspace{1cm}} & & \tabularnewline
    \hline
  \end{tabularx}
}

%
% Define headers and footers
%
\pagestyle{fancy}
\fancyhf{}
\lfoot{\thepage \hspace{1pt} / \pageref{LastPage}}
\rfoot{
\begin{tabular}{>{\raggedright}ll}
 Doc. Nr.: & \docNumber{}\tabularnewline
 Rev.: & \docRevision{}\tabularnewline
 Date: & \docDate{}\tabularnewline
 Doc. Type: & \docType{}\tabularnewline
\end{tabular}
}

\rhead{\includegraphics[width=8cm]{astron-logo}}
\cfoot{\textbf{\large{}\docProgram{}}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{1pt}
\footskip2cm

%
% Construct the title page, history, etc.
%
\renewcommand{\maketitle}{
\vspace*{4cm}
\begin{center}
\begin{minipage}{0.7\textwidth}
\centering\LARGE\textbf{\docTitle{}}
\end{minipage}
\end{center}

% This position was determined by trial and error; a bit unsatisfying.
\newlength{\copyrightBoxPosition}
\setlength{\copyrightBoxPosition}{25.2cm}

% Add the copyright notice towards the bottom of the page.
\begin{textblock*}{1\textwidth}(\sideMargin, \copyrightBoxPosition)
  \fbox{%
    \begin{minipage}{\dimexpr\linewidth-2\fboxrule-2\fboxsep}
      \begin{center}
        \begin{small}
          \ifthenelse{\equal{\vcsFirstYear}{\vcsLastYear}}
            {\textcopyright{} ASTRON \vcsFirstYear.\\}
            {\textcopyright{} ASTRON \vcsFirstYear--\vcsLastYear.\\}
          All rights are reserved. Reproduction in whole or in part is\\
          prohibited without written consent of the copyright owner.
        \end{small}
      \end{center}
    \end{minipage}%
  }
\end{textblock*}

% We generate the author table once (but don't print it) to measure its height,
% then use that to position the textblock* which ultimately contains the table.
% Note the measured height is (approximately) half the full height.
\newlength{\authorTableHalfHeight}
\settoheight{\authorTableHalfHeight}{\authorTable}

\if@hastype
  \newlength{\docTypeHalfHeight}
  \settoheight{\docTypeHalfHeight}{\docTypeDescription}
  \begin{textblock*}{1\textwidth}(\sideMargin,
                                  \dimexpr\copyrightBoxPosition-2\docTypeHalfHeight+\fboxsep)
  \docTypeDescription
  \end{textblock*}

  \begin{textblock*}{1\textwidth}(\sideMargin,
                                  \dimexpr\copyrightBoxPosition-2\docTypeHalfHeight-2\authorTableHalfHeight+2\fboxsep)
  \authorTable
  \end{textblock*}
\else
  \begin{textblock*}{1\textwidth}(\sideMargin,
                                  \dimexpr\copyrightBoxPosition-2\authorTableHalfHeight+\fboxsep)
  \authorTable
  \end{textblock*}
\fi


\clearpage

\changeRecord
}
